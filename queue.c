
//
//  queue.c
//
//
//  Created by Christof W Fetzer on 13.03.15 | KW 11.
//
//

#include "queue.h"

//
// init_queue
//
// init fields of a given queue_t structure q
//

void init_queue(struct queue_t* q) {
    q->push_index = 0;
    q->pull_index = 0;
}

//
// push message
//
// push message (msg) of size (size) into queue_t (q),
// if size > free space in queue, function returns -1.
//
// Return code:
//  0       OK - message was pushed in queue
//  -1      queue was full - could not push message in queue


int try_push_msg(struct queue_t* q, const char* msg, unsigned int size) {
    if (q->push_index+size > q->pull_index+QUEUE_LENGTH)
        return -1;
    while(size-- > 0) { // CF: slow copy ... rewrite if this is a problem
        q->buffer[q->push_index % QUEUE_LENGTH] = *msg++;
        q->push_index++;
    }
    return 0;
}

void push_msg(struct queue_t* q, const char* msg, unsigned int size) {
    int ret;
    do {
        ret = try_push_msg(q, msg, size);
    } while (-1 == ret);
}

//
// pull message - not reentrant - only one ouller permitted
//
// pull message (msg) of maximum size (max_size) from queue_t (q) into buffer (msg)
//
// Return code:
//  >0      number of bytes read
//  =0      no bytes read


int try_pull_msg(struct queue_t* q, char* msg, unsigned int max_size) {
    int ret = 0;
    while (ret < max_size && q->push_index > q->pull_index) {
        *msg++ = q->buffer[q->pull_index % QUEUE_LENGTH];
        q->pull_index++;
        ret++;
    }
    return ret;
    
}

void pull_msg(struct queue_t* q, char* msg, unsigned int size) {
    int read = 0;
    int total_read = 0;

    while (total_read < size) {
        read = try_pull_msg(q, msg + total_read, size - total_read);
        total_read += read;
    }
}
