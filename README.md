# README #

### What is this repository for? ###

This is a proof-of-concept application for our "remote" system call capability. main.c spawns two threads: one application thread and one system call thread. The application thread **never** executes any system calls. Instead, system calls are converted into messages, which are sent to the system call thread. The system call thread executes **all** system calls on behalf of the application thread.

The application thread code is contained in inmain.c:inmain(). The system call thread code is contained in outmain.c:outmain().

One use case of this "remote" system call mechanism is with Intel's upcoming Secure Guard Extensions (SGX) technology. With SGX, a thread running in "enclave mode" is not allowed to execute SYSENTER instructions, i.e., to perform a system call.

### How do I get set up? ###

```
#!bash

sudo apt-get update
sudo apt-get install gcc git strace make

cd
git clone https://tknauth@bitbucket.org/tknauth/remote-syscall-proof-of-concept.git
git clone https://tknauth@bitbucket.org/tknauth/musl.git musl-code

cd musl-code
./configure --prefix=$HOME/musl
make -j$(nproc)
make install

git checkout rsctest
./configure --prefix=$HOME/sgxmusl
make -j$(nproc)
make install

cd
cd remote-syscall-proof-of-concept
make
```

### How to test? ###

```
#!bash

~/remote-syscall-proof-of-concept/tiny
```

Will print something similar to the following:

```
TID 6475: this is the system call thread!
TID 6474: this is the web server thread!
```

In a separate console, use strace to monitor both threads

```
#!bash

sudo strace -p 6474 -p 6475
```

Now issue an HTTP request

```
#!bash

curl http://127.0.0.1:8080/remote-syscall-proof-of-concept/tiny.c
```

You will see that (almost) all system calls are executed by the system call thread, i.e., the lines are prefixed with "[pid  6475]". Currently, the web server thread still calls mmap() to allocate memory for its heap. Other system calls, e.g., writev, stat, open, and sendfile, are forwarded to and performed by the system call thread.

### Caveats ###

This is a hack! There are many ways in which this proof-of-concept is broken. If it runs as described, consider yourself lucky! ;)