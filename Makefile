SGXMUSL=$(HOME)/sgxmusl
MUSL=$(HOME)/musl

CFLAGS+=-g -ggdb

all : tiny alone

tiny : rsc_client.o rsc_server.o queue.o tiny_static.o
	$(MUSL)/bin/musl-gcc -g -static -o $@ $^ -lpthread

tiny_static.o : tiny.c
	$(SGXMUSL)/bin/musl-gcc $(CFLAGS) -c -o tiny.o $^
	objcopy --prefix-symbols sgx tiny.o
	ld -r -o $@ tiny.o $(SGXMUSL)/lib/libc.a
	objcopy --redefine-syms=tiny-redefine-syms $@

alone : tiny.c
	$(MUSL)/bin/musl-gcc -static -DALONE $^ -o $@

clean:
	$(RM) *.o simple tiny
