#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <assert.h>
#include <dlfcn.h>
#include <stdlib.h>

volatile int terminate = 0;

void* outmain(void*);
void* inmain(void*);

#define BUF_SIZE 1024

char* msg = "Hello, world!";

int main(int argc, char *argv[]) {
    pthread_t out_thread, in_thread;

    /* Initialize the Remote System Call Client (RSCC) side. Will
       eventually be removed or hidden somewhere else. */
    rscc_init_default();

    /* Start the enclave thread */
    assert(0 == pthread_create(&in_thread, NULL, inmain, NULL));
    /* Start the syscall thread */
    assert(0 == pthread_create(&out_thread, NULL, outmain, NULL));

    sleep(1);
    terminate = 1;

    /* Wait for enclave thread to exit. The syscall thread is
       terminated forcefully. */
    assert(0 == pthread_join(in_thread, NULL));

    return 0;
}
