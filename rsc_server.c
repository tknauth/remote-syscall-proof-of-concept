#include <assert.h>
#include <pthread.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "rsc_server.h"
#include "queue.h"

int terminate = 0;
struct queue_t* outq;
struct queue_t* inq;

struct response* do_accept(struct accept_request* req) {
    assert(req->syscallnr == SYS_accept);

    struct response* rep = malloc(sizeof(struct response));
    assert(rep != NULL);

    rep->ret = syscall(req->syscallnr, req->sockfd, req->addr, req->addrlen);
    return rep;
}

struct response* do_access(struct access_request* req) {
    assert(req->syscallnr == SYS_access);

    struct response* rep = malloc(sizeof(struct response));
    assert(rep != NULL);

    rep->ret = syscall(req->syscallnr, req->pathname, req->mode);
    return rep;
}

struct response* do_bind(struct bind_request* req) {
    assert(req->syscallnr == SYS_bind);

    struct response* rep = malloc(sizeof(struct response));
    assert(rep != NULL);

    rep->ret = syscall(req->syscallnr, req->sockfd, req->addr, req->addrlen);
    return rep;
}

struct response* do_close(struct close_request* req) {
    assert(req->syscallnr == SYS_close);

    struct response* rep = malloc(sizeof(struct response));
    assert(rep != NULL);

    rep->ret = syscall(req->syscallnr, req->fd);
    return rep;
}

struct response* do_fcntl(struct fcntl_request* req) {
    assert(req->syscallnr == SYS_fcntl);

    struct response* rep = malloc(sizeof(struct response));
    assert(rep != NULL);

    rep->ret = syscall(req->syscallnr, req->fd, req->cmd, req->arg);
    return rep;
}

struct response* do_ioctl(struct ioctl_request* req) {
    assert(req->syscallnr == SYS_ioctl);

    struct response* rep = malloc(sizeof(struct response));
    assert(rep != NULL);

    rep->ret = syscall(req->syscallnr, req->d, req->request, req->argp);
    return rep;
}

struct response* do_listen(struct listen_request* req) {
    assert(req->syscallnr == SYS_listen);

    struct response* rep = malloc(sizeof(struct response));
    assert(rep != NULL);

    rep->ret = syscall(req->syscallnr, req->sockfd, req->backlog);
    return rep;
}

struct response* do_open(struct open_request* req) {
    assert(req->syscallnr == SYS_open);

    struct response* rep = malloc(sizeof(struct response));
    assert(rep != NULL);

    rep->ret = syscall(req->syscallnr, req->pathname, req->flags, req->mode);
    return rep;
}

struct response* do_readv(struct readv_request* req) {
    assert(req->syscallnr == SYS_readv);

    struct response* rep = malloc(sizeof(struct response));
    assert(rep != NULL);

    rep->ret = syscall(req->syscallnr, req->fd, req->iov, req->iovcnt);
    return rep;
}

struct response* do_sendfile(struct sendfile_request* req) {
    assert(req->syscallnr == SYS_sendfile);

    struct response* rep = malloc(sizeof(struct response));
    assert(rep != NULL);

    rep->ret = syscall(req->syscallnr, req->out_fd, req->in_fd, req->offset, req->count);
    return rep;
}

struct response* do_setsockopt(struct setsockopt_request* req) {
    assert(req->syscallnr == SYS_setsockopt);

    struct response* rep = malloc(sizeof(struct response));
    assert(rep != NULL);

    rep->ret = syscall(req->syscallnr, req->sockfd, req->level, req->optname, req->optval, req->optlen);
    return rep;
}

struct response* do_socket(struct socket_request* req) {
    assert(req->syscallnr == SYS_socket);

    struct response* rep = malloc(sizeof(struct response));
    assert(rep != NULL);

    rep->ret = syscall(req->syscallnr, req->domain, req->type, req->protocol);
    return rep;
}

struct response* do_stat(struct stat_request* req) {
    assert(req->syscallnr == SYS_stat);

    struct response* rep = malloc(sizeof(struct response));
    assert(rep != NULL);

    rep->ret = syscall(req->syscallnr, req->path, req->buf);
    return rep;
}

struct response* do_write(struct write_request* req) {
    assert(req->syscallnr == SYS_write);

    struct response* rep = malloc(sizeof(struct response));
    assert(rep != NULL);

    rep->ret = syscall(req->syscallnr, req->fd, req->buf, req->count);
    return rep;
}

struct response* do_writev(struct writev_request* req) {
    assert(req->syscallnr == SYS_writev);

    struct response* rep = malloc(sizeof(struct response));
    assert(rep != NULL);

    rep->ret = syscall(req->syscallnr, req->fd, req->iov, req->iovcnt);
    return rep;
}

struct response* process_request(struct request* req) {
    switch (req->syscallnr) {
    case SYS_access:
        return do_access((struct access_request*) req);
    case SYS_accept:
        return do_accept((struct accept_request*) req);
    case SYS_bind:
        return do_bind((struct bind_request*) req);
    case SYS_close:
        return do_close((struct close_request*) req);
    case SYS_fcntl:
        return do_fcntl((struct fcntl_request*) req);
    case SYS_ioctl:
        return do_ioctl((struct ioctl_request*) req);
    case SYS_listen:
        return do_listen((struct listen_request*) req);
    case SYS_open:
        return do_open((struct open_request*) req);
    case SYS_readv:
        return do_readv((struct readv_request*) req);
    case SYS_sendfile:
        return do_sendfile((struct sendfile_request*) req);
    case SYS_setsockopt:
        return do_setsockopt((struct setsockopt_request*) req);
    case SYS_socket:
        return do_socket((struct socket_request*) req);
    case SYS_stat:
        return do_stat((struct stat_request*) req);
    case SYS_write:
        return do_write((struct write_request*) req);
    case SYS_writev:
        return do_writev((struct writev_request*) req);
    default:
        assert(0);
    }
}

void* server_main(void* params) {

    const char* msg = "TID %d: this is the system call thread!\n";
    char buf[1024];
    int len = sprintf(buf, msg, syscall(SYS_gettid));
    write(2, buf, len);

    while (!terminate) {
        struct request* req;
        pull_msg(outq, (char*) &req, sizeof(struct request*));
        struct response* resp = process_request(req);
        push_msg(inq, (char*) &resp, sizeof(struct response*));
    }
}

void* inmain(void* param);

int main(int argc, char* argv[]) {
    pthread_t out_thread, in_thread;

    inq = malloc(sizeof(struct queue_t));
    outq = malloc(sizeof(struct queue_t));

    assert(0 == pthread_create(&in_thread, NULL, inmain, NULL));
    assert(0 == pthread_create(&out_thread, NULL, server_main, NULL));

    /* sleep(1); */
    /* terminate = 1; */

    /* Wait for enclave thread to exit. The syscall thread is
       terminated forcefully. */
    assert(0 == pthread_join(in_thread, NULL));

    free(inq);
    free(outq);

    return 0;
}
