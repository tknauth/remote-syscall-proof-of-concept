#include <unistd.h>

struct request {
    int syscallnr;
};

struct accept_request {
    int syscallnr;
    int sockfd;
    struct sockaddr* addr;
    socklen_t* addrlen;
};

struct access_request {
    int syscallnr;
    const char* pathname;
    int mode;
};

struct bind_request {
    int syscallnr;
    int sockfd;
    const struct sockaddr* addr;
    socklen_t addrlen;
};

struct close_request {
    int syscallnr;
    int fd;
};

struct fcntl_request {
    int syscallnr;
    int fd;
    int cmd;
    long arg;
};

struct ioctl_request {
    int syscallnr;
    int d;
    int request;
    char* argp;
};

struct listen_request {
    int syscallnr;
    int sockfd;
    int backlog;
};

struct open_request {
    int syscallnr;
    const char* pathname;
    int flags;
    mode_t mode;
};

struct readv_request {
    int syscallnr;
    int fd;
    struct iovec* iov;
    int iovcnt;
};

struct sendfile_request {
    int syscallnr;
    int out_fd;
    int in_fd;
    off_t* offset;
    size_t count;
};

struct setsockopt_request {
    int syscallnr;
    int sockfd;
    int level;
    int optname;
    const void* optval;
    socklen_t optlen;
};

struct socket_request {
    int syscallnr;
    int domain;
    int type;
    int protocol;
};

struct stat_request {
    int syscallnr;
    const char* path;
    struct stat* buf;
};

struct write_request {
    int syscallnr;
    int fd;
    const void* buf;
    size_t count;
};

struct writev_request {
    int syscallnr;
    int fd;
    struct iovec* iov;
    int iovcnt;
};

struct response {
    int ret;
};
