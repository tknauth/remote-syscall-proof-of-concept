#include <assert.h>
#include <sys/syscall.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include "queue.h"
#include "rsc_server.h"

struct queue_t* inq;
struct queue_t* outq;

int rscc_accept(int sockfd, struct sockaddr* addr, socklen_t* addrlen) {
    struct accept_request* req = malloc(sizeof(struct accept_request));
    assert(req != NULL);
    req->syscallnr = SYS_accept;
    req->sockfd = sockfd;
    req->addr = addr;
    req->addrlen = addrlen;

    push_msg(outq, (char*) &req, sizeof(struct accept_request*));
    struct response* resp;
    pull_msg(inq, (char*) &resp, sizeof(struct response*));
    
    free(req);
    int ret = resp->ret;
    free(resp);
    return ret;
}

int rscc_access(const char* pathname, int mode) {
    struct access_request* req = malloc(sizeof(struct access_request));
    assert(req != NULL);
    req->syscallnr = SYS_access;
    req->pathname = pathname;
    req->mode = mode;

    push_msg(outq, (char*) &req, sizeof(struct access_request*));
    struct response* resp;
    pull_msg(inq, (char*) &resp, sizeof(struct response*));
    
    free(req);
    int ret = resp->ret;
    free(resp);
    return ret;
}

int rscc_bind(int sockfd, const struct sockaddr* addr, socklen_t addrlen) {
    struct bind_request* req = malloc(sizeof(struct bind_request));
    assert(req != NULL);
    req->syscallnr = SYS_bind;
    req->sockfd = sockfd;
    req->addr = addr;
    req->addrlen = addrlen;

    push_msg(outq, (char*) &req, sizeof(struct bind_request*));
    struct response* resp;
    pull_msg(inq, (char*) &resp, sizeof(struct response*));
    
    free(req);
    int ret = resp->ret;
    free(resp);
    return ret;
}

int rscc_close(int fd) {
    struct close_request* req = malloc(sizeof(struct close_request));
    assert(req != NULL);
    req->syscallnr = SYS_close;
    req->fd = fd;

    push_msg(outq, (char*) &req, sizeof(struct close_request*));
    struct response* resp;
    pull_msg(inq, (char*) &resp, sizeof(struct response*));
    
    free(req);
    int ret = resp->ret;
    free(resp);
    return ret;
}

int rscc_fcntl(int fd, int cmd, long arg) {
    struct fcntl_request* req = malloc(sizeof(struct fcntl_request));
    assert(req != NULL);
    req->syscallnr = SYS_fcntl;
    req->fd = fd;
    req->cmd = cmd;
    req->arg = arg;

    push_msg(outq, (char*) &req, sizeof(struct fcntl_request*));
    struct response* resp;
    pull_msg(inq, (char*) &resp, sizeof(struct response*));
    
    free(req);
    int ret = resp->ret;
    free(resp);
    return ret;
}

int rscc_ioctl(int d, int request, char* argp) {
    struct ioctl_request* req = malloc(sizeof(struct ioctl_request));
    assert(req != NULL);
    req->syscallnr = SYS_ioctl;
    req->d = d;
    req->request = request;
    req->argp = argp;

    push_msg(outq, (char*) &req, sizeof(struct ioctl_request*));
    struct response* resp;
    pull_msg(inq, (char*) &resp, sizeof(struct response*));
    
    free(req);
    int ret = resp->ret;
    free(resp);
    return ret;
}

int rscc_listen(int sockfd, int backlog) {
    struct listen_request* req = malloc(sizeof(struct listen_request));
    assert(req != NULL);
    req->syscallnr = SYS_listen;
    req->sockfd = sockfd;
    req->backlog = backlog;

    push_msg(outq, (char*) &req, sizeof(struct listen_request*));
    struct response* resp;
    pull_msg(inq, (char*) &resp, sizeof(struct response*));
    
    free(req);
    int ret = resp->ret;
    free(resp);
    return ret;
}

int rscc_open(const char* pathname, int flags, mode_t mode) {
    struct open_request* req;
    req = malloc (sizeof(struct open_request));

    req->syscallnr = SYS_open;
    req->pathname = pathname;
    req->flags = flags;
    req->mode = mode;

    push_msg(outq, (char*) &req, sizeof(struct open_request*));
    struct response* resp;
    pull_msg(inq, (char*) &resp, sizeof(struct response*));

    free(req);
    int ret = resp->ret;
    free(resp);
    return ret;
}

int rscc_sendfile(int out_fd, int in_fd, off_t* offset, size_t count) {
    struct sendfile_request* req;
    req = malloc (sizeof(struct sendfile_request));

    req->syscallnr = SYS_sendfile;
    req->out_fd = out_fd;
    req->in_fd = in_fd;
    req->offset = offset;
    req->count = count;

    push_msg(outq, (char*) &req, sizeof(struct sendfile_request*));
    struct response* resp;
    pull_msg(inq, (char*) &resp, sizeof(struct response*));

    free(req);
    int ret = resp->ret;
    free(resp);
    return ret;
}

int rscc_setsockopt(int sockfd, int level, int optname, const void* optval, socklen_t optlen) {
    struct setsockopt_request* req;
    req = malloc (sizeof(struct setsockopt_request));

    req->syscallnr = SYS_setsockopt;
    req->sockfd = sockfd;
    req->level = level;
    req->optname = optname;
    req->optval = optval;
    req->optlen = optlen;

    push_msg(outq, (char*) &req, sizeof(struct setsockopt_request*));
    struct response* resp;
    pull_msg(inq, (char*) &resp, sizeof(struct response*));

    free(req);
    int ret = resp->ret;
    free(resp);
    return ret;
}

int rscc_socket(int domain, int type, int protocol) {
    struct socket_request* req;
    req = malloc (sizeof(struct socket_request));

    req->syscallnr = SYS_socket;
    req->domain = domain;
    req->type = type;
    req->protocol = protocol;

    push_msg(outq, (char*) &req, sizeof(struct socket_request*));
    struct response* resp;
    pull_msg(inq, (char*) &resp, sizeof(struct response*));

    free(req);
    int ret = resp->ret;
    free(resp);
    return ret;
}

int rscc_stat(const char* path, struct stat* buf) {
    struct stat_request* req;
    req = malloc (sizeof(struct stat_request));

    req->syscallnr = SYS_stat;
    req->path = path;
    req->buf = buf;

    push_msg(outq, (char*) &req, sizeof(struct stat_request*));
    struct response* resp;
    pull_msg(inq, (char*) &resp, sizeof(struct response*));

    free(req);
    int ret = resp->ret;
    free(resp);
    return ret;
}

int rscc_readv(int fd, struct iovec* iov, int iovcnt) {
    struct readv_request* req;
    req = malloc (sizeof(struct readv_request));

    req->syscallnr = SYS_readv;
    req->fd = fd;
    req->iov = iov;
    req->iovcnt = iovcnt;

    push_msg(outq, (char*) &req, sizeof(struct readv_request*));
    struct response* resp;
    pull_msg(inq, (char*) &resp, sizeof(struct response*));

    free(req);
    int ret = resp->ret;
    free(resp);
    return ret;
}

int rscc_write(int fd, const void* buf, int count) {
    struct write_request* req;
    req = malloc (sizeof(struct write_request));

    req->syscallnr = SYS_write;
    req->fd = fd;
    req->buf = buf;
    req->count = count;

    push_msg(outq, (char*) &req, sizeof(struct write_request*));
    struct response* resp;
    pull_msg(inq, (char*) &resp, sizeof(struct response*));

    free(req);
    int ret = resp->ret;
    free(resp);
    return ret;
}

int rscc_writev(int fd, struct iovec* iov, int iovcnt) {
    struct writev_request* req;
    req = malloc (sizeof(struct writev_request));

    req->syscallnr = SYS_writev;
    req->fd = fd;
    req->iov = iov;
    req->iovcnt = iovcnt;

    push_msg(outq, (char*) &req, sizeof(struct writev_request*));
    struct response* resp;
    pull_msg(inq, (char*) &resp, sizeof(struct response*));

    free(req);
    int ret = resp->ret;
    free(resp);
    return ret;
}
