//
//  enclave.h
//
//
//  Created by Christof W Fetzer on 13.03.15 | KW 11.
//
//

#ifndef _queue_h
#define _queue_h

//
// Todo: maybe replace constant QUEUE_LENGTH by some more flexible mechanism
//
// Note: leave this a power of two - the compiler should then replace % by &

#define QUEUE_LENGTH        0x8000


//
// simple concurrent queue with one concurrent producer and one concurrent consumer
//

struct queue_t {
    unsigned long volatile  push_index;
    unsigned long volatile  pull_index;
    char volatile buffer[QUEUE_LENGTH];
};



//
// init_queue
//
// init fields of a given queue_t structure q
//


void init_queue(struct queue_t* q);

//
// push message - not reentrant - only one pusher permitted
//
// push message (msg) of size (size) into queue_t (q),
// if size > free space in queue, function returns -1.
//
// Return code:
//  0       OK - message was pushed in queue
//  -1      queue was full - could not push message in queue


int try_push_msg(struct queue_t* q, const char* msg, unsigned int size);

// Call try_push_msg repeatedly until it succeeds.
void push_msg(struct queue_t* q, const char* msg, unsigned int size);

//
// pull message - not reentrant - only one puller permitted
//
// pull message (msg) of maximum size (max_size) from queue_t (q) into buffer (msg)
//
// Return code:
//  >0      number of bytes read
//  =0      no bytes read

int try_pull_msg(struct queue_t* q, char* msg, unsigned int max_size);

// Pull a message of exact size from queue. Repeatedly calls
// try_pull_msg under the hood.
void pull_msg(struct queue_t* q, char* msg, unsigned int size);

#endif
